/*
 * SigList.cpp
 *
 *  Created on: Aug 11, 2019
 *      Author: OS1
 */

#include "SigList.h"
#include "PCB.h"

int SignalList::removeFirst()
{
	int signal = 0;
#ifndef BCC_BLOCK_IGNORE
		lock
	node* first = head;
	if (head != 0)
	{
		head = head->next;
		if (head == 0 ) tail = 0;
		signal = *((int*)(first->info));
		delete first->info;
		delete first;
	}
	unlock
#endif
	return signal;
}

void SignalList::addBackup(void* element)
{
#ifndef BCC_BLOCK_IGNORE
	lock
	node* newOne = new node(element);
	if (backupHead == 0)
		backupHead = backupTail = newOne;
	else
		backupTail = backupTail->next = newOne;
	unlock
#endif
}

void SignalList::replaceBackup()
{
#ifndef BCC_BLOCK_IGNORE
		lock
	head = backupHead; tail = backupTail;
	backupHead = 0; backupTail = 0;
	unlock
#endif
}
