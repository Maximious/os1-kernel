/*
 * PCB.cpp
 *
 *  Created on: Jul 24, 2019
 *      Author: OS1
 */

#include "PCB.h"
#include "KerSem.h"
#include "IVTEntry.h"
#include "DeadLTh.h"
#include "MThread.h"

//static fields
ID PCB::PID = 0;
int PCB::blockedSignalsGlobal[16] = {0};
TrList* PCB::ThreadList = new TrList;
long PCB::activeThreadCount = 0;
MainThread* PCB::mainThread = 0;
DeadLockThread* PCB::deadlockThread = DeadLockThread::createDeadLockThread();
volatile PCB* PCB::running = 0; //running thread
volatile int PCB::zahtevana_promena_konteksta = 0;
volatile int PCB::lockCounter = 0;
//constructor
PCB::PCB(StackSize stackSize, Time tSlice, Thread* thrd) : sp(0), ss(0),bp(0),done(0),
			timeSlice(tSlice), scheduled(0), currTime(tSlice), timeRunOut(0), blocked(0), infinityThread(0)
{
	int first = running == 0 ? 1 : 0;
	pid = PID++;
	pcbThread = thrd;
	threadSemaphore = new KernelSem(first);
	PCB::activeThreadCount++;
	father = (PCB*)PCB::running;
	ThreadList->add(thrd);

	//signals
	receivedSignals = new SignalList;
	handlers = new SignalHandlerList[16];

	copySignalStats(first);
	copySignalHandlers(first);

	if (tSlice == 0)
	{
		currTime = 0;
		infinityThread = 1;
	}

	if (stackSize > 32768 || stackSize < 24) //stack > 24 && < 64KB, sizeofunsigned = 2B
		stackSize = 32768;
	stackSize/=sizeof(unsigned); //unsigned = 16b

	stack = new unsigned[stackSize];

	#ifndef BCC_BLOCK_IGNORE //filling stack

		stack[stackSize - 1] = 0x200;//I flag
		stack[stackSize - 2] = FP_SEG(execute);
		stack[stackSize - 3] = FP_OFF(execute);

		sp = FP_OFF(stack + stackSize - 12);  //sp needs to save 12 registers
		ss = FP_SEG(stack + stackSize - 12);
		bp = FP_OFF(stack + stackSize - 12);

	#endif
}

//destructor
PCB::~PCB()
{
#ifndef BCC_BLOCK_IGNORE
	lock
	delete receivedSignals;
	delete[] handlers;
	delete stack;
	delete threadSemaphore;
	unlock
#endif
}

void PCB::start()
{
	if (!scheduled) // already started?
	{
		#ifndef BCC_BLOCK_IGNORE
		lock
			Scheduler::put(this);
		//	cout << "Stavljam u scheduler " << pid << endl;
			scheduled = 1;
		unlock
		#endif
	}
}

void dispatch() //synchronous context change
{
	if (PCB::lockCounter)
	{
		PCB::zahtevana_promena_konteksta = 1;
	}
	else
	{
		yield();
	}
}

void PCB::execute()
{
	running->pcbThread->run();
	running->endThread();
}

void PCB::waitToComplete()
{
	if(!done)
		threadSemaphore->wait();
}

void PCB::endThread() volatile //ovo da se zavrsiii
{
	if (done)
		return;
	#ifndef BCC_BLOCK_IGNORE
	lock
	//cout << "signaling end id:" << PCB::running->pid << endl;
	done = 1;
	PCB::activeThreadCount--;
	if (father)
			father->signal(1);
	signal(2);
	threadSemaphore->signal();
	unlock
	#endif
	dispatch();
}

Thread* PCB::getThreadById(ID id)
{
	return ThreadList->getThreadById(id);
}

//timer
pInterrupt oldTimer; // old routine
void tick();

void interrupt yield(...)
{
	#ifndef BCC_BLOCK_IGNORE
	lock
	asm{
			mov tsp, sp
			mov tss, ss
			mov tbp, bp
		}

	PCB::running->sp = tsp;
	PCB::running->ss = tss;
	PCB::running->bp = tbp;

			//cout<<"Promena konteksta! Brojac: "<<running->currTime << " Nit: " << running->pid
						//<< " Zahtev: " << zahtevana_promena_konteksta <<endl;

	if (!PCB::running->done && !PCB::running->blocked
			&& PCB::running->pcbThread != PCB::deadlockThread)
	{
		Scheduler::put((PCB*)PCB::running);
		//cout << "Stavljam u scheduler " <<PCB::running->pid << endl;
	}
	else
		PCB::running->unschedule();

	PCB::running = Scheduler::get();
	//cout << "Iz schedulera vadim " <<PCB::running->pid << endl;
	if (PCB::running == 0)
	{
		PCB::deadlockThread->unschedule();
	//	cout << "Entering deadlock" << endl;
	//	cout << "Active threads count: " << activeThreadCount << endl;
		PCB::deadlockThread->start();
		PCB::running = Scheduler::get();
	}

	tsp = PCB::running->sp;
	tss = PCB::running->ss;
	tbp = PCB::running->bp;

	PCB::running->currTime = PCB::running->timeSlice;
	PCB::zahtevana_promena_konteksta = 0;

	#ifndef BCC_BLOCK_IGNORE
	asm {
		mov sp, tsp   // restore sp
		mov ss, tss
		mov bp, tbp
	}
	#endif

	//cout << "RunningID: " << PCB::running->pid << endl;
	PCB::running->checkSignals();

	//cout << "ActiveThreads: " << activeThreadCount << endl;
	unlock
#endif
}

void interrupt PCB::timer(...)// prekidna rutina
{
		(*oldTimer)(); //calling old timer
		tick(); // tick for test
		KernelSem::updateBlockedThreads();
		if (PCB::running && PCB::running->currTime > 0)
			PCB::running->currTime--;
		if (PCB::running && ((PCB::running->currTime == 0 &&
				PCB::running->infinityThread == 0) || zahtevana_promena_konteksta))
		{
			if (lockCounter == 0)
			{
				yield();
			}
			else zahtevana_promena_konteksta = 1;
		}
}

void PCB::initMain()
{
	PCB::mainThread = MainThread::createMainThread();
	running = mainThread->myPCB;
}

void inic()
{
	#ifndef BCC_BLOCK_IGNORE
		lock
			timerEvent = new KernelEv(8);
			oldTimer = IVTEntry::entries[8]->getOldRoutine();
			IVTEntry::entries[8]->setOldRoutine(PCB::timer);
			PCB::initMain();
			cout << "Init done" << endl;
		unlock
	#endif
	dispatch();
}


void restore()
{
	#ifndef BCC_BLOCK_IGNORE
		lock
		IVTEntry::entries[8]->setOldRoutine(oldTimer);
		delete KernelSem::timedList;
		delete PCB::ThreadList;
		delete timerEvent;
		delete[] PCB::blockedSignalsGlobal;
		PCB::deadlockThread->destroy();
		PCB::mainThread->destroy();
		cout << "Restore done" << endl;
		unlock
	#endif
}

//for signals

void PCB::blockSignal(SignalId signal)
{
	blockedSignals[signal] = 1;
}

void PCB::blockSignalGlobally(SignalId signal)
{
	PCB::blockedSignalsGlobal[signal] = 1;
}

void PCB::unblockSignal(SignalId signal)
{
	blockedSignals[signal] = 0;
}

void PCB::unblockSignalGlobally(SignalId signal)
{
	PCB::blockedSignalsGlobal[signal] = 0;
}

void PCB::registerHandler(SignalId signal, SignalHandler handler)
{
	handlers[signal].add(handler);
}

void PCB::unregisterAllHandlers(SignalId id)
{
	handlers[id].removeAll();
}

void PCB::swap(SignalId id, SignalHandler hand1, SignalHandler hand2)
{
	(handlers[id]).swap(hand1,hand2);
}

void PCB::signal(SignalId signal) volatile
{
	if ((signal == 1 || signal == 2) && running->father != 0)// kernel is active when thread is finished
		return;
	if (signal == 2) //2 se mora odmah obraditi
	{
		if (!blockedSignals[2] && !blockedSignalsGlobal[2])
		{
			handlers[2].signal();
		}
	}
	int *num = new int(signal);
	receivedSignals->add(num);
}

void PCB::checkSignals() volatile
{
	int sigNumbr;
	while(receivedSignals->getHead() != 0)
	{
		sigNumbr = receivedSignals->removeFirst();
		if (!blockedSignals[sigNumbr] && !blockedSignalsGlobal[sigNumbr])
		{
			#ifndef BCC_BLOCK_IGNORE
			lock
			handlers[sigNumbr].signal();
			unlock
			#endif
		}
		else
		{
			int *num = new int(sigNumbr);
			receivedSignals->addBackup(num);
		}
	}
	receivedSignals->replaceBackup();
}

void zeroSignalHandler()
{
	//cout << "Zero signal handler activated!" << endl;
	PCB::running->endThread();
}

void PCB::copySignalHandlers(int first)
{
	if (!first)
	{
		for (int i = 0; i < 16; i++)
				handlers[i].copyList(running->handlers[i]);
	}
	else
	{
		registerHandler(0,zeroSignalHandler);
	}
}

void PCB::copySignalStats(int first)
{
#ifndef BCC_BLOCK_IGNORE
		lock
	if (!first)
	{
		for (int i = 0; i < 16; i++)
			blockedSignals[i] = running->blockedSignals[i];
	}
	else
	{
		for (int i = 0; i < 16; i++)
					blockedSignals[i] = 0;
	}
	unlock
	#endif
}
