/*
 * KerSem.cpp
 *
 *  Created on: Jul 30, 2019
 *      Author: OS1
 */

#include "KerSem.h"
#include "IVTEntry.h"

TimedList* KernelSem::timedList = new TimedList;

KernelSem::KernelSem(int val) : value(val)
{
	blockedInf = new LinkedList;
}

KernelSem::~KernelSem()
{
	#ifndef BCC_BLOCK_IGNORE
		lock
		delete blockedInf;
		blockedInf = 0;
		unlock
	#endif
}

int KernelSem::wait (Time maxTimeToWait)
{
	 int br = 0;
 	 #ifndef BCC_BLOCK_IGNORE
		 lock
		 if (--value < 0)
		 {
			 PCB::running->setTimeRunOut(0);
			 block(maxTimeToWait);
		 }
		 if (PCB::running->getTimeRunOut())
			 br = 1;
		 else
			 br = 0;
		 unlock
     #endif
	 return br;
}

void KernelSem::block (Time maxTimeToWait)
{
	if (maxTimeToWait > 0)
	{
		timedList->add(PCB::running->getThread(),maxTimeToWait,this);
	}
	else
	{
		blockedInf->add(PCB::running->getThread());
	}
	PCB::running->unschedule();
	PCB::running->block();
	#ifndef BCC_BLOCK_IGNORE
	unlock
	dispatch();
	lock
	#endif
}

int KernelSem::signal (int n)
{
	if (n == 0)
	{
	#ifndef BCC_BLOCK_IGNORE
			 lock
			// cout << "Value: " << value << endl;
			 if (value++<0)
			 {
				deblock();
			 }
			 unlock
	#endif
	}
	else if (n > 0)
	{
		int br = 0;
	#ifndef BCC_BLOCK_IGNORE
		lock
		while (br < n && deblock() != 0)
		{
			br++;
		}
		value+=n;
		unlock
	#endif
		return br;
	}
	else
	{
		return n;
	}
	return 0;
}


int KernelSem::deblock ()
{
	Thread* tr = (Thread*)(blockedInf->removeFirst());
	if (tr == 0)
	{
		tr = timedList->removeFromSem(this);
		if (tr == 0)
			return 0;
		tr->setTimeLeft(1);
	}
	tr->start();
	return 1;
}


void KernelSem::updateBlockedThreads()
{
#ifndef BCC_BLOCK_IGNORE
	lock
	TimedList::node *tr = timedList->head;
	if (tr == 0)
	{
		unlock
		return;
	}

	if (tr->timeLeft > 0)
		tr->timeLeft--;
	else
		cout << "Null timer;" << tr->timeLeft << endl;

	while (tr != 0 && tr->timeLeft == 0 )
	{
		tr->semaphore->value++;
		Thread* currThread = timedList->removeFirst();
		currThread->setTimeLeft(0); currThread->start();
		tr = timedList->head;
	}
	if (tr != 0 && tr->timeLeft > 10)
		cout << "Timer: " << tr->timeLeft << endl;
	if (tr != 0 && tr->timeLeft == 0)
			cout << "NUla je u pm" << tr->timeLeft << endl;
	//timedList->ispisiListu();
	unlock
#endif
}

