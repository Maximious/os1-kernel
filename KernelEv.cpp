/*
 * KernelEv.cpp
 *
 *  Created on: Aug 3, 2019
 *      Author: OS1
 */

#include "KernelEv.h"
#include "PCB.h"

KernelEv::KernelEv(unsigned char ivtNo)
{
	entry = (IVTEntry::entries)[ivtNo];
	entry->setEvent(this);
	owner = (PCB*)PCB::running;
	semaphore = new KernelSem(0);
}

KernelEv::~KernelEv()
{
	delete entry;
	delete semaphore;
}

void KernelEv::signal ()
{
#ifndef BCC_BLOCK_IGNORE
	/*
	if (entry!= 0 && entry->getNumb() == 9)
		cout << "Key snapped sem value" << semaphore->val() << endl;*/
	semaphore->signal();
	lock
	semaphore->setToBlock();
	dispatch(); // posle signala obavezno preuzimanje
	unlock
#endif
}

void KernelEv::wait()
{
	if (PCB::running == owner)
	{
		semaphore->wait(0);
	}
}

