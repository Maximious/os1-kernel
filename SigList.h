/*
 * SigList.h
 *
 *  Created on: Aug 11, 2019
 *      Author: OS1
 */

#ifndef SIGLIST_H_
#define SIGLIST_H_

#include "LList.h"

class SignalList : public LinkedList
{
	node *backupHead, *backupTail;
public:
	SignalList() : LinkedList(),
		backupHead(0), backupTail(0) {}
	int removeFirst();
	void addBackup(void* element);
	void replaceBackup();
};


#endif /* SIGLIST_H_ */
