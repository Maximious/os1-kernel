/*
 * IVTEntry.cpp
 *
 *  Created on: Aug 3, 2019
 *      Author: OS1
 */

#include "IVTEntry.h"

IVTEntry* IVTEntry::entries[256];

IVTEntry::IVTEntry(int interruptNo,pInterrupt routine) : intNo(interruptNo), event(0), oldRoutine(0)
{
#ifndef BCC_BLOCK_IGNORE
	oldRoutine = getvect(interruptNo);
	setvect(interruptNo,routine);
#endif
	event = 0;
	entries[intNo] = this;
}

IVTEntry::~IVTEntry()
{
#ifndef BCC_BLOCK_IGNORE
	lock
	setvect(intNo, oldRoutine);
	entries[intNo] = 0;
	event = 0;
	unlock
#endif
}

void IVTEntry::execute(int num)
{
	if (num == 1)
	{
		//cout << "Event occurred" << endl;
		(*oldRoutine)();
	}
	event->signal();
}


