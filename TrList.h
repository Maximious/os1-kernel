/*
 * TrList.h
 *
 *  Created on: Jul 25, 2019
 *      Author: OS1
 */

#ifndef TRLIST_H_
#define TRLIST_H_


#include "LList.h"

class TrList : public LinkedList
{
public:
	TrList() : LinkedList() {};
	Thread* getThreadById(ID id);
	Thread* removeFirst();
};

#endif /* TRLIST_H_ */
