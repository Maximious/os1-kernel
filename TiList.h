/*
 * TiList.h
 *
 *  Created on: Jul 31, 2019
 *      Author: OS1
 */

#ifndef TILIST_H_
#define TILIST_H_

#include "Thread.h"
#include "KerSem.h"

class KernelSem;

class TimedList
{
	struct node
	{
		Thread* info;
		node* next;
		Time timeLeft;
		KernelSem* semaphore; // blocked on this semaphore
		node(Thread* el, Time time, KernelSem* sem) :
			info(el), timeLeft(time), next(0), semaphore(sem)
		{
		};
	};
protected:
	node *head;
	node *tail;
public:
	friend class KernelSem;
	TimedList() : head(0), tail(0) {}
	~TimedList();
	node* getHead() { return head; }
	node* getTail() { return tail; }
	void add(Thread* element, Time time, KernelSem* sem);
	Thread* removeFirst();
	Thread* removeFromSem(KernelSem* sem);
	void ispisiListu();
};



#endif /* TILIST_H_ */


