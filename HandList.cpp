/*
 * HandList.cpp
 *
 *  Created on: Aug 10, 2019
 *      Author: OS1
 */

#include "HandList.h"
#include "PCB.h"

void SignalHandlerList::removeAll()
{
#ifndef BCC_BLOCK_IGNORE
	lock
	while (head != 0)
		removeFirst();
	unlock
#endif
}

void SignalHandlerList::swap(SignalHandler hand1,
		SignalHandler hand2)
{
#ifndef BCC_BLOCK_IGNORE
	lock
	node *node1 = 0, *node2 = 0, *cur = head;
	while (cur != 0 && (node1 == 0 || node2 == 0))
	{
		if (node1 == 0)
		{
			if (cur->info == hand1)
				node1 = cur;
		}
		if (node2 == 0)
		{
			if (cur->info == hand2)
				node2 = cur;
		}
		cur = cur->next;
	}

	if (node1 == 0 || node2 == 0)
	{
		unlock
		return;
	}
	node1->info = hand2; node2->info = hand1;
	unlock
#endif
}

void SignalHandlerList::signal()
{
#ifndef BCC_BLOCK_IGNORE
	node *cur = head;
	while (cur != 0)
	{
		SignalHandler func = (SignalHandler)(cur->info);
		lock
		(*func)();
		unlock
		cur = cur->next;
	}
#endif
}

void SignalHandlerList::copyList(const SignalHandlerList &list)
{
#ifndef BCC_BLOCK_IGNORE
	lock
	node* cur = list.head;
	while( cur != 0 )
	{
		add(cur->info);
		cur = cur -> next;
	}
	unlock
#endif
}

