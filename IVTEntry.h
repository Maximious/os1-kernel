/*
 * IVTEntry.h
 *
 *  Created on: Aug 3, 2019
 *      Author: OS1
 */

#ifndef IVTENTRY_H_
#define IVTENTRY_H_

#include "KerSem.h"
#include "DOS.H"
#include "KernelEv.h"
#include "Thread.h"

const int NumOfInterruptEntries = 256 ; // Number of interrupt entries

class KernelEv;

class IVTEntry
{
public:
	static IVTEntry *entries[256];
private:
	pInterrupt oldRoutine;
	int intNo;
	KernelEv* event; // samo jedan dogadjaj ide na jedan ulaz
public:
	IVTEntry(int interruptNo, pInterrupt routine);
	~IVTEntry();
	int getNumb() {return intNo;}
	void execute(int num); // to call oldRoutine
	void setEvent(KernelEv* kEv) { event = kEv; }
	pInterrupt getOldRoutine() { return oldRoutine; }
	void setOldRoutine(pInterrupt pInter) { oldRoutine = pInter; }
};


#endif /* IVTENTRY_H_ */
