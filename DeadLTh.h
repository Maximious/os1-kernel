/*
 * DeadLTh.h
 *
 *  Created on: Aug 12, 2019
 *      Author: OS1
 */

#ifndef DEADLTH_H_
#define DEADLTH_H_

#include "PCB.h"
#include "Thread.h"

class DeadLockThread : public Thread
{
private:
	static DeadLockThread* singleton;
	DeadLockThread ( StackSize stackSize = 50, Time timeSlice = 2 )
				: Thread(stackSize, timeSlice)  {}
public:
	static DeadLockThread* createDeadLockThread();
	void destroy();
protected:
	virtual void run();

};


#endif /* DEADLTH_H_ */
