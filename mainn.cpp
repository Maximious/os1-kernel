#include "PCB.h"
#include <STDIO.H>
#include "KerSem.h"
#include "KernelEv.h"
#include "IVTEntry.h"
#include "user.h"
#include "Event.h"

int userMain (int argc, char* argv[]);

PREPAREENTRY(8,1);

int main(int argc, char* argv[])
{
	int status;
	inic();

	status = userMain(argc,argv);

	restore();
	return status;
}


