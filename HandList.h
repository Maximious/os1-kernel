/*
 * HandlerrList.h
 *
 *  Created on: Aug 10, 2019
 *      Author: OS1
 */

#ifndef HANDLIST_H_
#define HANDLIST_H_

#include "LList.h"
#include "Thread.h"

class SignalHandlerList : public LinkedList
{

public:
	SignalHandlerList() : LinkedList() {}
	void copyList(const SignalHandlerList &list);
	void removeAll();
	void swap(SignalHandler hand1, SignalHandler hand2);
	void signal();
};

#endif /* HANDLIST_H_ */
