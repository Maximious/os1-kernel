/*
 * DeadLTh.cpp
 *
 *  Created on: Aug 12, 2019
 *      Author: OS1
 */

#include "DeadLTh.h"
#include "PCB.h"

DeadLockThread* DeadLockThread::singleton = 0;

void DeadLockThread::run()
{
	int br = 0;
	while (1)
	{
 		br++;
	}
}

DeadLockThread* DeadLockThread::createDeadLockThread()
{
	#ifndef BCC_BLOCK_IGNORE
		if (singleton == 0)
		{
			lock
			singleton = new DeadLockThread();
			unlock
		}
	#endif
		return singleton;
}

void DeadLockThread::destroy()
{
	#ifndef BCC_BLOCK_IGNORE
	lock
	if (singleton != 0)
		delete singleton;
	singleton = 0;
	unlock
#endif
}

