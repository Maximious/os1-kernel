/*
 * thread.cpp
 *
 *  Created on: Jul 25, 2019
 *      Author: OS1
 */
#include "PCB.h"
#include "Thread.h"
#include "TrList.h"

void Thread::start()
{
	myPCB->start();
	myPCB->unblock();
}

void Thread::waitToComplete()
{
	 myPCB->waitToComplete();
}

ID Thread::getId()
{
	return myPCB->getID();
}

ID Thread::getRunningId()
{
	return PCB::getRunningPID();
}

Thread * Thread::getThreadById(ID id)
{
	return PCB::getThreadById(id);
}

Thread::Thread (StackSize stackSize, Time timeSlice)
{
	myPCB = new PCB(stackSize, timeSlice, this);
}

Thread::~Thread()
{
	//myPCB->waitToComplete();
	delete myPCB;
}

void Thread::blockSignal(SignalId signal)
{
	myPCB->blockSignal(signal);
}

void Thread::blockSignalGlobally(SignalId signal)
{
	PCB::blockSignalGlobally(signal);
}

void Thread::unblockSignal(SignalId signal)
{
	myPCB->unblockSignal(signal);
}

void Thread::unblockSignalGlobally(SignalId signal)
{
	PCB::unblockSignalGlobally(signal);
}

void Thread::signal(SignalId signal)
{
	myPCB->signal(signal);
}

void Thread::registerHandler(SignalId signal, SignalHandler handler)
{
	myPCB->registerHandler(signal, handler);
}


void Thread::unregisterAllHandlers(SignalId id)
{
	myPCB->unregisterAllHandlers(id);
}

void Thread::swap(SignalId id, SignalHandler hand1, SignalHandler hand2)
{
	myPCB->swap(id,hand1,hand2);
}

void Thread::setTimeLeft(int val)
{
	myPCB->setTimeRunOut(val);
}

void Thread::unschedule()
{
	myPCB->unschedule();
}

