/*
 * LList.h
 *
 *  Created on: Jul 30, 2019
 *      Author: OS1
 */

#ifndef LLIST_H_
#define LLIST_H_

#include "Thread.h"
#include "IOSTREAM.H"

class LinkedList
{
	struct node
	{
		void* info;
		node* next;
		node(void* el) : info(el), next(0) {};
	};
protected:
	node *head;
	node *tail;
public:
	LinkedList() : head(0), tail(0) {}
	~LinkedList();
	node* getHead() { return head; }
	node* getTail() { return tail; }
	void add(void* element);
	void* removeFirst();
	void ispisi();
};




#endif /* LLIST_H_ */
