/*
 * MainThread.h
 *
 *  Created on: Aug 18, 2019
 *      Author: OS1
 */

#ifndef MTHREAD_H_
#define MTHREAD_H_

#include "PCB.h"
#include "Thread.h"

class MainThread : public Thread
{
private:
	static MainThread* singleton;
	MainThread ();
public:
	static MainThread* createMainThread();
	void destroy();
protected:
 virtual void run();

};

#endif /* MTHREAD_H_ */
