/*
 * TrList.cpp
 *
 *  Created on: Jul 25, 2019
 *      Author: OS1
 */
#include "TrList.h"
#include "PCB.h"

Thread* TrList::getThreadById(ID id)
{
	node* curr = head;
#ifndef BCC_BLOCK_IGNORE
	lock
	while (curr && ((Thread*)(curr->info))->getId() != id)
	{
		curr = curr->next;
	}
	unlock
#endif
	return (Thread*)(curr->info);
}

Thread* TrList::removeFirst()
{
	return (Thread*)LinkedList::removeFirst();
}
