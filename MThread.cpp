/*
 * MainThread.cpp
 *
 *  Created on: Aug 18, 2019
 *      Author: OS1
 */

#include "MThread.h"

#include "PCB.h"

MainThread* MainThread::singleton = 0;


MainThread:: MainThread()
	: Thread(defaultStackSize,0)
{
}

void MainThread::run()
{

}

MainThread* MainThread::createMainThread()
{
	#ifndef BCC_BLOCK_IGNORE
		if (singleton == 0)
		{
			lock
			singleton = new MainThread();
			unlock
		}
	#endif
		return singleton;
}

void MainThread::destroy()
{
#ifndef BCC_BLOCK_IGNORE
	lock
	if (singleton != 0)
		delete singleton;
	singleton = 0;
	unlock
#endif
}


