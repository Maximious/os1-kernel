/*
 * event.cpp
 *
 *  Created on: Aug 3, 2019
 *      Author: OS1
 */

#include "Event.h"
#include "KernelEv.h"

Event::Event (IVTNo ivtNo)
{
	myImpl = new KernelEv(ivtNo);
}

Event::~Event ()
{
#ifndef BCC_BLOCK_IGNORE
	lock
		delete myImpl;
	unlock
#endif
}

void Event::wait ()
{
	myImpl -> wait();
}

void Event::signal()
{
	myImpl -> signal();
}
