/*
 * KerSem.h
 *
 *  Created on: Jul 30, 2019
 *      Author: OS1
 */

#ifndef KERSEM_H_
#define KERSEM_H_

#include "TiList.h"
#include "PCB.h"

class TimedList;

class KernelSem
{

private:
	 int value;
	 LinkedList  *blockedInf;  //without timer
	 void setToBlock() { value = 0; }
public:
	 friend class KernelEv;
	 static TimedList *timedList; //with timer
	 KernelSem (int val);
	 ~KernelSem();
	 int wait (Time maxTimeToWait = 0);
	 int signal(int n=0);
	 static void updateBlockedThreads();
	 LinkedList* getBlocked() { return blockedInf; }
	 int val () const { return value; }
protected:
	 void block (Time maxTimeToWait);
	 int deblock ();
};


#endif /* KERSEM_H_ */
