/*
 * LList.cpp

 *
 *  Created on: Jul 30, 2019
 *      Author: OS1
 */

#include "LList.h"
#include "PCB.h"

void LinkedList::add(void* element)
{
#ifndef BCC_BLOCK_IGNORE
	lock
		node* newOne = new node(element);
		if (head == 0)
			head = tail = newOne;
		else
			tail = tail->next = newOne;
	unlock
#endif
}

void* LinkedList::removeFirst()
{
	void* tr = 0;
#ifndef BCC_BLOCK_IGNORE
	lock
		node* first = head;
		if (head != 0)
		{
			head = head->next;
			if (head == 0 ) tail = 0;
			tr = first->info;
			first->next = 0;
			delete first;
		}
	unlock
#endif
	return tr;
}

LinkedList::~LinkedList()
{
	node* tr=head, *pr = 0;
	while(tr!=0)
	{
		pr=tr;
		tr = tr-> next;
		delete pr;
	}
	head=tail=0;
}

void LinkedList::ispisi()
{
#ifndef BCC_BLOCK_IGNORE
	lock
	int br = 0;
	for (node* currNode = head; currNode != 0;
			currNode = currNode->next)
	{
		cout << "Nit br: " << ((Thread*)(currNode->info))->getId() << endl;
		br++;
	}
	cout << endl;
unlock
#endif
}
