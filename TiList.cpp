/*
 * TiList.cpp
 *
 *  Created on: Jul 31, 2019
 *      Author: OS1
 */


#include "TiList.h"
#include "PCB.h"

void TimedList::add(Thread* element, Time time, KernelSem* sem)
{
#ifndef BCC_BLOCK_IGNORE
	lock
	node* newOne = new node(element,time,sem);
	if (head == 0)
		head = tail = newOne;
	else
	{
		node* tr = head, *pr = 0;
		while (tr!= 0 && tr->timeLeft <= newOne->timeLeft)
		{
			newOne->timeLeft -= tr->timeLeft;
			pr = tr;
			tr = tr->next;
		}
		if (pr == 0) // na pocetku
		{
			newOne->next = head;
			head = newOne;
		}
		else if ( tr == 0 ) // na kraju
		{
			tail = tail->next = newOne;
		}
		else // u sredini
		{
			newOne->next = tr;
			pr->next = newOne;
		}
		if (tr != 0)
			tr->timeLeft -= newOne->timeLeft;
	}
	unlock
#endif
}

Thread* TimedList::removeFirst()
{
	Thread* tr = 0;
#ifndef BCC_BLOCK_IGNORE
	lock
	node* first = head;
	if (first != 0)
	{
		head = head->next;
		if (head == 0 ) tail = 0;
		else head->timeLeft += first->timeLeft;
		tr = first->info;
		first->next = 0;
		delete first;
	}
	unlock
#endif
	return tr;
}

Thread* TimedList::removeFromSem(KernelSem* sem)
{
	Thread* tr = 0;
#ifndef BCC_BLOCK_IGNORE
	lock
	//cout << "Removing violently!" << endl;
	node *curr = head, *prev = 0;
	if (curr != 0)
	{
		while (curr != 0 && curr->semaphore != sem)
		{
			prev = curr;
			curr = curr->next;
		}
		if (curr == 0) //not found
			return 0;
		else if (prev == 0) //first
		{
			head = head->next;
			if (head == 0 ) tail = 0;
			else head->timeLeft += curr->timeLeft;
		}
		else if (curr->next == 0) //last
		{
			tail = prev;
			tail->next = 0;
		}
		else //middle
		{
			prev->next = curr->next;
			curr->next->timeLeft+=curr->timeLeft;
		}

		tr = curr->info;
		curr->semaphore = 0;
		curr->next = 0;
		delete curr;
		curr = 0;
	}
	unlock
#endif
	return tr;
}

TimedList::~TimedList()
{
#ifndef BCC_BLOCK_IGNORE
	lock
	node* tr=head, *pr = 0;
	while(tr!=0)
	{
		pr=tr;
		tr = tr-> next;
		delete pr;
	}
	head=tail=0;
	unlock
#endif
}

void TimedList::ispisiListu()
{
#ifndef BCC_BLOCK_IGNORE
	lock
	for (node* currNode = head; currNode != 0;
			currNode = currNode->next)
	//	if (currNode->timeLeft > 10 )
			cout << "Vrednost brojaca: " <<
			currNode->timeLeft << endl;
	cout << endl;
	unlock
#endif
}

