/*
 * PCB.h
 *
 *  Created on: Jul 24, 2019
 *      Author: OS1
 */

#ifndef PCB_H_
#define PCB_H_

#include <IOSTREAM.H>
#include <dos.h>
#include "schedule.h"
#include "TrList.h"
#include "HandList.h"
#include "SigList.h"
#include "KerSem.h"
#include "DeadLTh.h"
#include "MThread.h"
#include "Thread.h"

#ifndef BCC_BLOCK_IGNORE
// block context switch
#define lock asm cli; \
			 PCB::lockCounter--; \
			 asm sti;

// allow context switch
#define unlock asm cli; \
			  PCB::lockCounter++; \
			  PCB::lockCounter = PCB::lockCounter >= 0 ? 0 : PCB::lockCounter; \
			  asm sti;\
			  if (PCB::zahtevana_promena_konteksta) dispatch();
#endif

typedef void interrupt (*pInterrupt)(...); //interrupt functions


class KernelEv;
class KernelSem;
class DeadLockThread;
class MainThread;

//variables for timer interrupt function
	static unsigned tsp; // temporary sp, ss, bp
	static unsigned tss;
	static unsigned tbp;

	static KernelEv *timerEvent;

class PCB
{

protected:

	//pcb attributes
		ID pid; //process id
		unsigned scheduled; //bool, is it in Scheduelr???
		unsigned done; //endOfThread
		unsigned blocked; //if blocked on sem
		unsigned sp; //stackPointer
		unsigned ss; //stackSegment
		unsigned bp; //basePointer
		unsigned* stack;
		Time timeSlice; //max time till context switch
		int timeRunOut;
		Thread* pcbThread; // pointer to a thread object containing this pcb
		KernelSem *threadSemaphore;
		PCB* father;

		static void initMain();

	// for signals
		void copySignalStats(int first = 0); //first thread
		void copySignalHandlers(int first = 0);
		SignalHandlerList* handlers;
		SignalList* receivedSignals;
		void checkSignals() volatile;
		static int blockedSignalsGlobal[16];
		int blockedSignals[16];
public:

		static volatile int lockCounter;

		friend void interrupt yield(...); //switch context
		friend void dispatch();
		friend void inic();
		friend void restore();

	// pcb functions
		PCB(StackSize stackSize, Time tSlice, Thread* thrd);
		~PCB();
		void start();
		void waitToComplete();
		void endThread() volatile;
		ID getID() {return pid;};
		static ID getRunningPID()	{ return running->pid;};
		static Thread* getThreadById(ID id);

	//static fields
		static TrList* ThreadList;
		static volatile int zahtevana_promena_konteksta;
		static volatile PCB* running;
		static ID PID; //each thread has ID
		static long activeThreadCount;
		static DeadLockThread* deadlockThread; //when every thread is blocked
		static MainThread* mainThread;

		static void execute(); // every thread calls this function


	//context switch
		static void interrupt timer(...); // timer interrupt routine
		Time currTime;//how much time left till context switch
		int infinityThread; // no time limit

	//helping functions
	void unschedule() volatile { scheduled = 0; } //removed from scheduler
	Thread* getThread()  volatile { return pcbThread; } //get thread containing this PCB


	//when semaphore blocks a thread
	void block() volatile { blocked = 1; }
	void unblock() { blocked = 0; }
	// da li je nit nastavila zbog timera na semaforu ili ne????
	int getTimeRunOut() volatile { return timeRunOut; }
	void setTimeRunOut(int val) volatile  { timeRunOut = val; }

	//for signals
	void blockSignal(SignalId signal);
	static void blockSignalGlobally(SignalId signal);
	void unblockSignal(SignalId signal);
	static void unblockSignalGlobally(SignalId signal);
	void signal(SignalId signal) volatile;
	void registerHandler(SignalId signal, SignalHandler handler);
	void unregisterAllHandlers(SignalId id);
	void swap(SignalId id, SignalHandler hand1, SignalHandler hand2);

};



#endif /* PCB_H_ */
