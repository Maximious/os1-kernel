/*
 * KernelEv.h
 *
 *  Created on: Aug 3, 2019
 *      Author: OS1
 */

#ifndef KERNELEV_H_
#define KERNELEV_H_

#include "KerSem.h"
#include "IVTEntry.h"
#include "Thread.h"

#define PREPAREENTRY(entryNum,callOldRoutine); \
		void interrupt routine##entryNum(...); \
		IVTEntry* entry##entryNum = new IVTEntry(entryNum, routine##entryNum); \
		void interrupt routine##entryNum(...)\
		{ \
			IVTEntry::entries[entryNum] -> execute(callOldRoutine);\
		};

class IVTEntry;

class KernelEv
{
private:
	 KernelSem* semaphore;
	 IVTEntry* entry;
	 PCB* owner;
public:
	KernelEv(unsigned char ivtNo);
	~KernelEv();
	void wait();
	void signal ();
	IVTEntry* getIVTEntry() { return entry; }
};


#endif /* KERNELEV_H_ */
